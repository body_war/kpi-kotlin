package ua.kpi.its.lab.security.ui.main

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.*
import ua.kpi.its.lab.security.dto.SoftwareProductRequestDto
import ua.kpi.its.lab.security.dto.SoftwareProductResponseDto

@Composable
fun SoftwareScreen(
    token: String,
    scope: CoroutineScope,
    client: HttpClient,
    snackbarHostState: SnackbarHostState
) {
    var softwareProducts by remember { mutableStateOf<List<SoftwareProductResponseDto>>(listOf()) }
    var loading by remember { mutableStateOf(false) }
    var openDialog by remember { mutableStateOf(false) }
    var selectedSoftwareProduct by remember { mutableStateOf<SoftwareProductResponseDto?>(null) }

    LaunchedEffect(token) {
        loading = true
        delay(1000)
        softwareProducts = withContext(Dispatchers.IO) {
            try {
                val response = client.get("http://localhost:8080/api/products") {
                    bearerAuth(token)
                }
                loading = false
                response.body()
            } catch (e: Exception) {
                val msg = e.toString()
                snackbarHostState.showSnackbar(msg, withDismissAction = true, duration = SnackbarDuration.Indefinite)
                softwareProducts
            }
        }
    }

    if (loading) {
        LinearProgressIndicator(
            color = MaterialTheme.colorScheme.primary,
            modifier = Modifier.fillMaxWidth()
        )
    }
    Scaffold(
        floatingActionButton = {
            FloatingActionButton(
                onClick = {
                    selectedSoftwareProduct = null
                    openDialog = true
                },
                content = {
                    Icon(Icons.Filled.Add, "add software product")
                }
            )
        }
    ) {
        if (softwareProducts.isEmpty()) {
            Column(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.Center
            ) {
                Text("No software products to show", modifier = Modifier.fillMaxWidth(), textAlign = TextAlign.Center)
            }
        } else {
            LazyColumn(
                modifier = Modifier.background(MaterialTheme.colorScheme.surfaceVariant).fillMaxSize().padding(16.dp),
                verticalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                items(softwareProducts) { softwareProduct ->
                    SoftwareItem(
                        softwareProduct = softwareProduct,
                        onEdit = {
                            selectedSoftwareProduct = softwareProduct
                            openDialog = true
                        },
                        onRemove = {
                            scope.launch {
                                withContext(Dispatchers.IO) {
                                    try {
                                        val response = client.delete("http://localhost:8080/api/products/${softwareProduct.id}") {
                                            bearerAuth(token)
                                        }
                                        require(response.status.isSuccess())
                                    } catch (e: Exception) {
                                        val msg = e.toString()
                                        snackbarHostState.showSnackbar(msg, withDismissAction = true, duration = SnackbarDuration.Indefinite)
                                    }
                                }

                                loading = true

                                softwareProducts = withContext(Dispatchers.IO) {
                                    try {
                                        val response = client.get("http://localhost:8080/api/products") {
                                            bearerAuth(token)
                                        }
                                        loading = false
                                        response.body()
                                    } catch (e: Exception) {
                                        val msg = e.toString()
                                        snackbarHostState.showSnackbar(msg, withDismissAction = true, duration = SnackbarDuration.Indefinite)
                                        softwareProducts
                                    }
                                }
                            }
                        }
                    )
                }
            }
        }

        if (openDialog) {
            SoftwareDialog(
                softwareProduct = selectedSoftwareProduct,
                token = token,
                scope = scope,
                client = client,
                onDismiss = {
                    openDialog = false
                },
                onError = {
                    scope.launch {
                        snackbarHostState.showSnackbar(it, withDismissAction = true, duration = SnackbarDuration.Indefinite)
                    }
                },
                onConfirm = {
                    openDialog = false
                    loading = true
                    scope.launch {
                        softwareProducts = withContext(Dispatchers.IO) {
                            try {
                                val response = client.get("http://localhost:8080/api/products") {
                                    bearerAuth(token)
                                }
                                loading = false
                                response.body()
                            } catch (e: Exception) {
                                loading = false
                                softwareProducts
                            }
                        }
                    }
                }
            )
        }
    }
}

@Composable
fun SoftwareDialog(
    softwareProduct: SoftwareProductResponseDto?,
    token: String,
    scope: CoroutineScope,
    client: HttpClient,
    onDismiss: () -> Unit,
    onError: (String) -> Unit,
    onConfirm: () -> Unit,
) {
    var name by remember { mutableStateOf(softwareProduct?.name ?: "") }
    var developer by remember { mutableStateOf(softwareProduct?.developer ?: "") }
    var version by remember { mutableStateOf(softwareProduct?.version ?: "") }
    var releaseDate by remember { mutableStateOf(softwareProduct?.releaseDate ?: "") }
    var size by remember { mutableStateOf(softwareProduct?.size?.toString() ?: "") }
    var bitness by remember { mutableStateOf(softwareProduct?.bitness ?: "") }
    var crossPlatform by remember { mutableStateOf(softwareProduct?.crossPlatform ?: false) }

    Dialog(onDismissRequest = onDismiss) {
        Card(modifier = Modifier.padding(16.dp).wrapContentSize()) {
            Column(
                modifier = Modifier.padding(16.dp, 8.dp).width(IntrinsicSize.Max).verticalScroll(rememberScrollState()),
                verticalArrangement = Arrangement.spacedBy(8.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                if (softwareProduct == null) {
                    Text("Create Software Product")
                } else {
                    Text("Update Software Product")
                }

                HorizontalDivider()
                TextField(name, { name = it }, label = { Text("Name") })
                TextField(developer, { developer = it }, label = { Text("Developer") })
                TextField(version, { version = it }, label = { Text("Version") })
                TextField(releaseDate, { releaseDate = it }, label = { Text("Release Date") })
                TextField(size, { size = it }, label = { Text("Size") })
                TextField(bitness, { bitness = it }, label = { Text("Bitness") })
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Switch(crossPlatform, { crossPlatform = it })
                    Spacer(modifier = Modifier.width(16.dp))
                    Text("Cross Platform")
                }

                HorizontalDivider()
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Button(
                        modifier = Modifier.weight(1f),
                        onClick = onDismiss,
                        colors = ButtonDefaults.buttonColors(containerColor = MaterialTheme.colorScheme.error)
                    ) {
                        Text("Cancel")
                    }
                    Spacer(modifier = Modifier.fillMaxWidth(0.1f))
                    Button(
                        modifier = Modifier.weight(1f),
                        onClick = {
                            scope.launch {
                                try {
                                    val request = SoftwareProductRequestDto(
                                        name, developer, version, releaseDate, size.toDouble(), bitness, crossPlatform
                                    )
                                    val response = if (softwareProduct == null) {
                                        client.post("http://localhost:8080/api/products") {
                                            bearerAuth(token)
                                            setBody(request)
                                            contentType(ContentType.Application.Json)
                                        }
                                    } else {
                                        client.put("http://localhost:8080/api/products/${softwareProduct.id}") {
                                            bearerAuth(token)
                                            setBody(request)
                                            contentType(ContentType.Application.Json)
                                        }
                                    }
                                    require(response.status.isSuccess())
                                    onConfirm()
                                } catch (e: Exception) {
                                    val msg = e.toString()
                                    onError(msg)
                                }
                            }
                        }
                    ) {
                        if (softwareProduct == null) {
                            Text("Create")
                        } else {
                            Text("Update")
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun SoftwareItem(softwareProduct: SoftwareProductResponseDto, onEdit: () -> Unit, onRemove: () -> Unit) {
    Card(shape = CardDefaults.elevatedShape, elevation = CardDefaults.elevatedCardElevation()) {
        ListItem(
            overlineContent = {
                Text(softwareProduct.name)
            },
            headlineContent = {
                Text(softwareProduct.version)
            },
            supportingContent = {
                Text("${softwareProduct.developer} - ${softwareProduct.size} MB")
            },
            trailingContent = {
                Row(modifier = Modifier.padding(0.dp, 20.dp), verticalAlignment = Alignment.CenterVertically) {
                    Icon(
                        Icons.Default.Edit,
                        contentDescription = "Edit",
                        tint = MaterialTheme.colorScheme.primary,
                        modifier = Modifier.clip(CircleShape).clickable(onClick = onEdit)
                    )
                    Spacer(modifier = Modifier.width(16.dp))
                    Icon(
                        Icons.Default.Delete,
                        contentDescription = "Remove",
                        tint = MaterialTheme.colorScheme.primary,
                        modifier = Modifier.clip(CircleShape).clickable(onClick = onRemove)
                    )
                    Spacer(modifier = Modifier.width(16.dp))
                }
            }
        )
    }
}
