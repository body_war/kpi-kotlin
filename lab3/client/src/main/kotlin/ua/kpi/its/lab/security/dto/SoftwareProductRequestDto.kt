package ua.kpi.its.lab.security.dto

import kotlinx.serialization.Serializable

@Serializable
data class SoftwareProductRequestDto(
    val name: String,
    val developer: String,
    val version: String,
    val releaseDate: String,
    val size: Double,
    val bitness: String,
    val crossPlatform: Boolean
)
